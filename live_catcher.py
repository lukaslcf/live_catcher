from bs4 import BeautifulSoup
import requests
from datetime import datetime
from datetime import timedelta
from time import sleep
import youtube_dl

# youtube link to channel
yt_channel = "https://www.youtube.com/channel/UCq-ylUNa9RoTK5jr6TBOYag/videos?view=2&flow=grid"
# start time of the live
live_time_str = "2020/1/18 00:00:00"
# seconds before next fetch
fetch_delay = 10
# path/folder to save file
video_path = "E:\\Lukas\\Video\\"
# format of file
o_format = r"%(uploader)s_%(upload_date)s_%(id)s_%(autonumber)s.%(ext)s"

link_prefix = "https://www.youtube.com"

live_time = datetime.strptime(live_time_str, "%Y/%m/%d %H:%M:%S")
video_f = ""
if ".mp4" in video_path:
    video_f = video_path
else:
    video_f = video_path + o_format

print("Time to start recording: " + live_time_str)
while datetime.now() < live_time:
    s = "\rWaiting..." + "(" + str(live_time - datetime.now()) + ")"
    print(s, end='')
    sleep(fetch_delay)

# fetching the link of the live video
page_html = requests.get(yt_channel)
page_bs = BeautifulSoup(page_html.content, "html.parser")
content_box = page_bs.select_one("li.channels-content-item.yt-shelf-grid-item")
is_live = content_box.find("span", string="Live now")
counter = 0
while not is_live:
    counter = counter + 1
    page_html = requests.get(yt_channel)
    page_bs = BeautifulSoup(page_html.content, "html.parser")
    content_box = page_bs.select_one("li.channels-content-item.yt-shelf-grid-item")
    is_live = content_box.find("span", string="Live now")
    print("\rNot yet live" + " (" + str(counter * fetch_delay) + ")", end='')
    sleep(fetch_delay)

link_to_vid = content_box.select_one("h3.yt-lockup-title").find('a')['href']
link_to_vid = link_prefix + link_to_vid

print("link found: " + link_to_vid)
print("Downloading with youtube-dl")

# 6 hours time detection to restart download in case of token change (not tested)
start_time = datetime.now()
curr_time = start_time + timedelta(hours=6)
counter = 1
ydl_opts = {
    'outtmpl': video_f,
    'hls_use_mpegts': True
}

with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    while curr_time - start_time >= timedelta(hours=6):
        ydl.download([link_to_vid])

        counter = counter + 1
        curr_time = datetime.now()
